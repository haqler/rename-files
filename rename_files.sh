#!/bin/bash
# Mon Dec  4 14:47:02 EST 2017


#output=rename_out
output="&1"
usage="Usage: rename.sh -f file_with_names -d parent_directory"


while [[ -n "$1" ]]; do
	case "$1" in
		'-d')
			shift
			work_dir=$1
			;;

		'-f')
			shift
			in_file=$1
			;;

		'-o')
			shift
			output_file="$1"
			;;

		*)
			echo "Incorrect parameters." >&2
			echo "$usage" >&2
			exit 1
			;;
	esac

	shift
done


if [[ -z "$work_dir" ]]; then
	echo "Parent directory missing. Exit." >&2
	echo "$usage" >&2
	exit 1
fi

if [[ -z "$in_file" ]]; then
	echo "File with names missing. Exit." >&2
	echo "$usage" >&2
	exit 1
fi

if [[ -z "$output_file" ]]; then
	exec 3>&1

else
	exec 3>$output_file
	echo "Output to file '$output_file'"

fi


echo "Start"
echo $(date) >&3


n=1
while IFS= read line; do
	echo >&3
	echo "Line $n: $line" >&3
	((n++))

	before=${line%;*}
	after=${line#*;}
	echo "BEFORE=$before" >&3
	echo "AFTER=$after" >&3
	if [[ -z "$before" || -z "$after" || "$before" == "$after" ]]; then
		echo "Incorrect values in line $n. Next." >&3
		continue
	fi
	
	
	all_dirs="$(find "$work_dir" -name "$before" -type d)"
	if [[ -z "$all_dirs" ]]; then
		echo "File '$before' not found in '$work_dir'. Next." >&3
		continue
	fi

	IFS=$'\n'
	for tmp in $all_dirs; do
		new_name="$(dirname "$tmp")/"$after""
		if [[ -d "$new_name" ]]; then
			echo "File '$new_name' already exist." >&3
			echo "File '$tmp' not renamed! Next." >&3
			continue
		fi
		

		echo "Rename "$tmp" to "$new_name""
		printf "%s\t---->\t%s\n" "$tmp" "$new_name" >&3
		mv "$tmp" "$new_name"
	done

done <"$in_file"

echo "Done"

